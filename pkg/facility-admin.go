package exocomp

import (
	"context"
	"fmt"
	"log"
	"strings"

	"gitlab.com/mergetb/tech/cogs/admin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

type FacilityAdmin struct {
	config FacilityAdminConfig
}

type FacilityAdminConfig struct {
	AdminConfig
	Facility string
}

func NewFacilityAdmin(cfg FacilityAdminConfig) *FacilityAdmin {

	return &FacilityAdmin{
		config: cfg,
	}

}

func (a *FacilityAdmin) CheckNoVMs(mzid string) {

	a.config.Test.Logf("checking vms are gone")

	a.call(func(ac admin.AdminClient) error {

		resp, err := ac.ListVMs(context.TODO(), &admin.ListVMsRequest{})
		if err != nil {
			a.config.Test.Fatalf("vm list read error: %v", err)
		}

		for _, name := range resp.Vms {

			if strings.Contains(name, mzid) {
				a.config.Test.Errorf("dangling vm: %s", name)
			}

		}

		return nil

	})

}

func (a *FacilityAdmin) call(f func(admin.AdminClient) error) error {

	creds, err := credentials.NewClientTLSFromFile(a.config.Cert, a.config.Facility)
	if err != nil {
		log.Fatalf("bad cert: %v", err)
	}

	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", a.config.Facility, 6001),
		grpc.WithTransportCredentials(creds),
	)

	if err != nil {
		log.Fatalf("dial: %v", err)
	}

	defer conn.Close()

	return f(admin.NewAdminClient(conn))

}
