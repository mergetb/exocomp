package exocomp

/*

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type Account struct {
	Username string
	Email    string
	Password string
}

type Components struct {
	Accounts []*Account
	PA       *PortalAdmin
	FA       *FacilityAdmin
	Cli      *CmdLineFatal // Fatal only. Use Cli.Cmd() if you want non-fatal

	PACert string
	FACert string

	Facility string

	Assert *assert.Assertions
}

func (c *Components) InitComponents(t *testing.T) {

	if c.PACert == "" {
		c.PACert = "merge-admin.pem"
	}

	if c.FACert == "" {
		c.FACert = "facility-admin.pem"
	}

	if c.Facility == "" {
		c.Facility = "spineleaf.mergetb.test"
	}

	if len(c.Accounts) == 0 {
		c.Accounts = defaultAccounts()
	}

	c.PA = NewPortalAdmin(
		AdminConfig{
			Test: t,
			Cert: c.PACert,
		},
	)

	c.FA = NewFacilityAdmin(
		FacilityAdminConfig{
			AdminConfig: AdminConfig{
				Test: t,
				Cert: c.FACert,
			},
			Facility: c.Facility,
		},
	)

	for _, u := range c.Accounts {

		c.Cli = NewCmdLineFatal(
			Config{
				Test:     t,
				User:     u.Username,
				Password: u.Password,
				Project:  u.Username,
			},
		)

		// Create the auth account on the internal portal auth server
		c.PA.CreateAccount(u.Username, u.Email, u.Password)
		// Login to the portal and "join" it, adding the account to the portal users
		c.Cli.Init()
		// Acctivate the new account on the portal
		c.PA.ActivateAccount(u.Username)
		// logout the user
		c.Cli.Cmd().Logout()
	}

	// Log the last user back in
	c.Cli.Init()

	c.Assert = assert.New(t)
}

func defaultAccounts() []*Account {

	// Last account is the one that is signed into the Cli post Init.
	as := []Account{
		{"picard", "picard@starfleet.upf.gov", "thereR4LightsLOL"},
		{"reg", "reg@starfleet.upf.gov", "endicott^2"},
	}

	accts := []*Account{}
	for _, a := range as {
		accts = append(accts, &Account{a.Username, a.Email, a.Password})
	}

	return accts
}
*/
