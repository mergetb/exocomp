package exocomp

/*

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/mergetb/engine/api/admin"
	//"gitlab.com/mergetb/engine/api/alloc"
	xp "gitlab.com/mergetb/engine/api/experimenter"
	xirv3 "gitlab.com/mergetb/xir/v0.3/go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

type PortalAdmin struct {
	config      AdminConfig
	unwindStack []func()
}

func NewPortalAdmin(cfg AdminConfig) *PortalAdmin {

	return &PortalAdmin{
		config: cfg,
	}

}

func (pa *PortalAdmin) unwind(f func()) {
	pa.unwindStack = append([]func(){f}, pa.unwindStack...)
}

func (pa *PortalAdmin) Unwind() {
	pa.config.Test.Logf("unwinding")

	for _, f := range pa.unwindStack {
		f()
	}
}

*/

//*****************************************************************************
// Allocation tests
//*****************************************************************************

/*
func (a *PortalAdmin) CheckAllocationTable() {

	a.config.Test.Logf("checking allocation table")

	a.call(func(ac admin.AdminClient) error {

		tbl, err := ac.Fetch(context.TODO(), &xp.FetchAllocationRequest{})
		if err != nil {
			a.config.Test.Fatalf("allocation table read error: %v", err)
		}

		for resource, allocations := range tbl.Table.Resource {
			for _, alloc := range allocations.Value {

				a.checkResourceAlloc(resource, alloc)

			}
		}

		return nil

	})

}

func (a *PortalAdmin) CheckNoAllocs(rzid string) {

	a.config.Test.Logf("checking for leaked allocations for %s", rzid)

	a.call(func(ac admin.AdminClient) error {

		tbl, err := ac.Fetch(context.TODO(), &alloc.Empty{})
		if err != nil {
			a.config.Test.Fatalf("allocation table read error: %v", err)
		}

		for _, allocations := range tbl.Table.Resource {
			for _, alloc := range allocations.Value {

				if alloc.Mzid == rzid {
					a.config.Test.Errorf("allocation for %s leaked", alloc.Node)
				}

			}
		}

		return nil

	})

}

func (a *PortalAdmin) checkResourceAlloc(
	resource string, alloc *xirv3.ResourceAllocation) {

	a.config.Test.Logf("checking %s:%s.%s", resource, alloc.Node, alloc.Mzid)

	if alloc.Mzid == "" {
		a.config.Test.Errorf("resource `%s` allocation for `%s` has no mzid",
			resource, alloc.Node)
	}

}
*/

//*****************************************************************************
// Account CRUD
//*****************************************************************************

/*
func (a *PortalAdmin) CreateAccount(uid, email, password string) {

	a.config.Test.Logf("creating portal account %s", uid)

	a.call(func(ac admin.AdminClient) error {

		resp, err := ac.CreateAccount(
			context.TODO(),
			&admin.CreateAccountRequest{
				Username: uid,
				Email:    email,
				Password: password,
			},
		)
		if err != nil {
			a.config.Test.Fatal(err)
		}

		if len(resp.Errors) > 0 {
			for _, e := range resp.Errors {
				a.config.Test.Logf(e)
			}
			a.config.Test.Fatal("Account create failed")
		}

		a.unwind(func() { a.DeleteAccount(uid) })

		return nil
	})
}

func (a *PortalAdmin) AccountExists(uid string) bool {

	a.config.Test.Logf("checking portal account %s", uid)

	exists := false
	a.call(func(ac admin.AdminClient) error {

		accts, err := ac.ListAcccounts(context.TODO(), &admin.ListAccountsRequest{})
		if err != nil {
			a.config.Test.Fatal(err)
		}

		for _, a := range accts.Accounts {
			if a.Username == uid {
				exists = true
				return nil
			}
		}

		// a.config.Test.Fatalf("account %s does not exist", uid)
		return nil
	})

	return exists
}

func (a *PortalAdmin) DeleteAccount(uid string) {

	a.config.Test.Logf("deleting portal account %s", uid)

	a.call(func(ac admin.AdminClient) error {

		resp, err := ac.DeleteAccount(
			context.TODO(),
			&admin.DeleteAccountRequest{
				Username: uid,
			},
		)
		if err != nil {
			a.config.Test.Fatal(err)
		}

		if resp.Success == false {
			a.config.Test.Fatal("Delete Account failed")
		}

		return nil
	})
}

func (a *PortalAdmin) ActivateAccount(uid string) {

	a.config.Test.Logf("activating account %s", uid)

	a.call(func(ac admin.AdminClient) error {

		_, err := ac.ActivateAccount(
			context.TODO(),
			&admin.ActivateAccountRequest{
				Username: uid,
			},
		)
		if err != nil {
			a.config.Test.Fatal(err)
		}

		a.unwind(func() { a.DeactivateAccount(uid) })

		return nil
	})
}

func (a *PortalAdmin) DeactivateAccount(uid string) {

	a.config.Test.Logf("deactivating account %s", uid)

	a.call(func(ac admin.AdminClient) error {

		_, err := ac.FreezeAccount(
			context.TODO(),
			&admin.FreezeAccountRequest{
				Username: uid,
			},
		)
		if err != nil {
			a.config.Test.Fatal(err)
		}

		return nil
	})
}

//
//
//
func (a *PortalAdmin) call(f func(admin.AdminClient) error) error {

	creds, err := credentials.NewClientTLSFromFile(a.config.Cert, "admin.mergetb.net")
	if err != nil {
		log.Fatalf("bad cert: %v", err)
	}

	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", "admin.mergetb.net", 6000),
		grpc.WithTransportCredentials(creds),
	)

	if err != nil {
		log.Fatalf("dial: %v", err)
	}

	defer conn.Close()

	return f(admin.NewAdminClient(conn))

}
*/
