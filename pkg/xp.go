package exocomp

import (
	"context"
	"fmt"

	"gitlab.com/mergetb/engine/api/xp"
	"google.golang.org/grpc"
)

type XpUnit struct {
	Config Config

	unwindStack []func()
	token       string
}

func NewXpUnit(cfg Config) *XpUnit {
	return &XpUnit{
		Config: cfg,
	}
}

func (xpu *XpUnit) GetUsers() ([]*xp.User, error) {

	var users []*xp.User
	err := xpu.workspace(func(cli xp.WorkspaceClient) error {

		resp, err := cli.GetUsers(context.TODO(), &xp.GetUsersRequest{})
		if err != nil {
			return fmt.Errorf("get users: %v", err)
		}

		users = resp.Users
		return nil

	})
	return users, err

}

func (xpu *XpUnit) Register(username, email, password string) error {

	err := xpu.identity(func(cli xp.IdentityClient) error {

		_, err := cli.Register(context.TODO(), &xp.RegisterRequest{
			Username: username,
			Password: password,
			Email:    email,
		})
		if err != nil {
			return fmt.Errorf("register: %v", err)
		}

		return nil

	})

	if err != nil {
		return err
	}

	xpu.unwind(func() { xpu.Unregister(username) })

	return nil

}

func (xpu *XpUnit) Unregister(username string) error {

	return xpu.identity(func(cli xp.IdentityClient) error {

		_, err := cli.Unregister(context.TODO(), &xp.UnregisterRequest{
			Username: username,
		})
		if err != nil {
			return fmt.Errorf("unregister: %v", err)
		}

		return nil

	})

}

func (xpu *XpUnit) Login(username, password string) error {

	return xpu.identity(func(cli xp.IdentityClient) error {

		resp, err := cli.Login(context.TODO(), &xp.LoginRequest{
			Username: username,
			Password: password,
		})
		if err != nil {
			return fmt.Errorf("login: %v", err)
		}
		xpu.token = resp.Token

		return nil

	})

}

func (xpu *XpUnit) workspace(f func(xp.WorkspaceClient) error) error {

	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", xpu.Config.Server, xpu.Config.Port),
		grpc.WithInsecure(),
	)
	if err != nil {
		return fmt.Errorf("grpc dial: %v", err)
	}
	defer conn.Close()

	cli := xp.NewWorkspaceClient(conn)

	return f(cli)

}

func (xpu *XpUnit) identity(f func(xp.IdentityClient) error) error {

	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", xpu.Config.Server, xpu.Config.Port),
		grpc.WithInsecure(),
	)
	if err != nil {
		return fmt.Errorf("grpc dial: %v", err)
	}
	defer conn.Close()

	cli := xp.NewIdentityClient(conn)

	return f(cli)

}

func (xpu *XpUnit) unwind(f func()) {

	xpu.unwindStack = append([]func(){f}, xpu.unwindStack...)

}

func (xpu *XpUnit) Unwind() {

	xpu.Config.Test.Logf("unwinding")

	for _, f := range xpu.unwindStack {
		f()
	}

}
