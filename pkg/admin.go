package exocomp

import (
	"testing"
)

type AdminConfig struct {
	Test     *testing.T
	Cert     string
	User     string
	Password string
}
