package exocomp

import (
	"fmt"
	"testing"

	"gitlab.com/mergetb/mergeapi"
	xirv3 "gitlab.com/mergetb/xir/v0.3/go"
)

var ma = &mergeapi.MergeAPI{
	AppName: "mergetb",
	URL:     "api.mergetb.net",
}

type Config struct {
	Test     *testing.T
	User     string
	Server   string
	Port     uint
	Password string
	Project  string
	Auth     string
	Cert     string
}

type Experiment struct {
	Name    string
	Project string
	Net     *xirv3.Network
}

type Realization struct {
	Name       string
	Experiment *Experiment
}

type Materialization struct {
	Realization *Realization
}

func (rz *Realization) Rzid() string {

	return fmt.Sprintf("%s.%s.%s",
		rz.Name,
		rz.Experiment.Name,
		rz.Experiment.Project,
	)

}
