package exocomp

import (
	"encoding/json"
	"fmt"
	"os/exec"
	"time"

	"gitlab.com/mergetb/engine/api/portal/models"
	"gitlab.com/mergetb/engine/pkg/merror"
)

type CmdLineFatal struct {
	cmd *CmdLine
}

type CmdLine struct {
	Config      Config
	unwindStack []func()
}

func NewCmdLine(cfg Config) *CmdLine {
	return &CmdLine{
		Config: cfg,
	}
}

func NewCmdLineFatal(cfg Config) *CmdLineFatal {
	return &CmdLineFatal{
		cmd: NewCmdLine(cfg),
	}
}

func (xoc *CmdLineFatal) Cmd() *CmdLine {
	return xoc.cmd
}

func (xoc *CmdLine) Init() error {

	return xoc.login()

}

func (xoc *CmdLineFatal) Init() {

	err := xoc.cmd.Init()
	if err != nil {
		xoc.cmd.Config.Test.Fatal(err)
	}

}

func (xoc *CmdLine) login() error {

	if xoc.Config.Auth == "" {
		xoc.Config.Auth = "internal"
	}

	out, err := exec.Command(
		"mergetb", "login",
		"-a", xoc.Config.Auth,
		xoc.Config.User,
		"--passwd", xoc.Config.Password,
	).CombinedOutput()

	if err != nil {
		return xoc.handleCmdError(out, err)
	}

	xoc.Config.Test.Logf("logged in %s", xoc.Config.User)

	xoc.unwind(func() { xoc.Logout() })

	// This could be a separate step/function, but is probably OK here.
	out, err = exec.Command("mergetb", "join").CombinedOutput()
	if err != nil {
		return fmt.Errorf("join failed: %w", err)
	}

	// there is no un-join in the API to unwind althuogh there should be.
	return nil
}

func (xoc *CmdLine) Logout() error {

	xoc.Config.Test.Logf("logging out %s", xoc.Config.User)

	out, err := exec.Command("mergetb", "logout").CombinedOutput()
	if err != nil {
		return xoc.handleCmdError(out, err)
	}

	return nil
}

func (xoc *CmdLine) NewExperiment(name string) (*Experiment, error) {

	xoc.Config.Test.Logf("creating experiment %s", name)

	out, err := exec.Command(
		"mergetb", "new", "experiment", name).CombinedOutput()

	if err != nil {
		return nil, xoc.handleCmdError(out, err)
	}

	xoc.unwind(func() { xoc.DeleteExperiment(name) })

	return &Experiment{
		Name:    name,
		Project: xoc.Config.Project,
	}, nil

}

func (xoc *CmdLine) ShowExperiment(name string) (*models.Experiment, error) {

	out, err := exec.Command(
		"mergetb", "show", "experiment", name, "-j",
	).CombinedOutput()
	if err != nil {
		return nil, xoc.handleCmdError(out, err)
	}

	exp := &models.Experiment{}
	err = json.Unmarshal(out, &exp)
	if err != nil {
		return nil, xoc.handleCmdError(out, err)
	}

	return exp, nil
}

func (xoc *CmdLineFatal) NewExperiment(name string) *Experiment {

	xp, err := xoc.cmd.NewExperiment(name)
	if err != nil {
		xoc.cmd.Config.Test.Fatal(err)
	}

	return xp
}

// func (xoc *CmdLine) ProjectMembers

func (xoc *CmdLine) DeleteExperiment(name string) error {

	xoc.Config.Test.Logf("deleting experiment %s", name)

	out, err := exec.Command(
		"mergetb", "delete", "experiment", name).CombinedOutput()

	if err != nil {
		return xoc.handleCmdError(out, err)
	}

	return nil
}

func (xoc *CmdLineFatal) DeleteExperiment(name string) {

	err := xoc.cmd.DeleteExperiment(name)
	if err != nil {
		xoc.cmd.Config.Test.Fatal(err)
	}

}

func (xoc *CmdLine) Push(xp *Experiment, src string) error {

	xoc.Config.Test.Logf("pushing experiment %s", xp.Name)

	out, err := exec.Command(
		"mergetb", "push", xp.Name, src).CombinedOutput()
	if err != nil {
		return xoc.handleCmdError(out, err)
	}

	mdl, err := ma.CompileModelFile(src)
	if err != nil {
		return err
	}

	xp.Net = mdl

	return nil

}

func (xoc *CmdLineFatal) Push(xp *Experiment, src string) {

	err := xoc.cmd.Push(xp, src)
	if err != nil {
		xoc.cmd.Config.Test.Fatal(err)
	}

}

func (xoc *CmdLine) Realize(xp *Experiment, name string) (*Realization, error) {

	rz := &Realization{
		Name:       name,
		Experiment: xp,
	}

	xoc.Config.Test.Logf("realizing %s", rz.Rzid())

	out, err := exec.Command(
		"mergetb", "realize", xp.Name, name).CombinedOutput()
	if err != nil {
		return nil, xoc.handleCmdError(out, err)
	}

	xoc.unwind(func() {
		xoc.Free(rz)
		xoc.Wait(rz, Free)
	})

	return rz, nil

}

func (xoc *CmdLineFatal) Realize(xp *Experiment, name string) *Realization {

	rz, err := xoc.cmd.Realize(xp, name)
	if err != nil {
		xoc.cmd.Config.Test.Fatal(err)
	}

	return rz

}

func (xoc *CmdLine) Free(rz *Realization) error {

	xoc.Config.Test.Logf("freeing %s", rz.Rzid())

	out, err := exec.Command(
		"mergetb", "free", rz.Experiment.Name, rz.Name).CombinedOutput()
	if err != nil {
		return xoc.handleCmdError(out, err)
	}

	return nil

}

func (xoc *CmdLineFatal) Free(rz *Realization) {

	err := xoc.cmd.Free(rz)
	if err != nil {
		xoc.cmd.Config.Test.Fatal(err)
	}

}

func (xoc *CmdLine) Materialize(rz *Realization) (*Materialization, error) {

	xoc.Config.Test.Logf("materializing %s", rz.Rzid())

	out, err := exec.Command(
		"mergetb", "materialize", rz.Experiment.Name, rz.Name).CombinedOutput()
	if err != nil {
		return nil, xoc.handleCmdError(out, err)
	}

	mz := &Materialization{
		Realization: rz,
	}

	xoc.unwind(func() {
		xoc.Dematerialize(mz)
		xoc.Wait(rz, Dematerialize)
	})

	return mz, nil

}

func (xoc *CmdLineFatal) Materialize(rz *Realization) *Materialization {

	mz, err := xoc.cmd.Materialize(rz)
	if err != nil {
		xoc.cmd.Config.Test.Fatal(err)
	}

	return mz

}

func (xoc *CmdLine) Dematerialize(mz *Materialization) error {

	xoc.Config.Test.Logf("dematerializing %s", mz.Realization.Rzid())

	out, err := exec.Command(
		"mergetb",
		"dematerialize",
		mz.Realization.Experiment.Name,
		mz.Realization.Name,
	).CombinedOutput()

	if err != nil {
		return xoc.handleCmdError(out, err)
	}

	return nil

}

func (xoc *CmdLineFatal) Dematerialize(mz *Materialization) {

	err := xoc.cmd.Dematerialize(mz)
	if err != nil {
		xoc.cmd.Config.Test.Fatal(err)
	}

}

type WaitTarget string

const (
	Materialize   WaitTarget = "materialize"
	Free          WaitTarget = "free"
	Dematerialize WaitTarget = "dematerialize"
)

func (xoc *CmdLine) Wait(rz *Realization, target WaitTarget) error {

	xoc.Config.Test.Logf(
		"waiting on %s for %s.%s", target, rz.Name, rz.Experiment.Name)

	out, err := exec.Command(
		"mergetb",
		"wait",
		string(target),
		rz.Experiment.Name,
		rz.Name,
	).CombinedOutput()

	if err != nil {
		return xoc.handleCmdError(out, err)
	}

	return nil

}

func (xoc *CmdLineFatal) Wait(rz *Realization, target WaitTarget) {

	err := xoc.cmd.Wait(rz, target)
	if err != nil {
		xoc.cmd.Config.Test.Fatal(err)
	}

}

func (xoc *CmdLine) Attach(rz *Realization) error {

	xoc.Config.Test.Logf("attaching %s", rz.Rzid())

	out, err := exec.Command(
		"xdc",
		"attach",
		xoc.Config.Project,
		rz.Experiment.Name,
		rz.Name,
	).CombinedOutput()

	if err != nil {
		return fmt.Errorf("attach failed: %v: %s", err, string(out))
	}

	xoc.unwind(func() { xoc.Detach(rz) })

	return nil

}

func (xoc *CmdLineFatal) Attach(rz *Realization) {

	err := xoc.cmd.Attach(rz)
	if err != nil {
		xoc.cmd.Config.Test.Fatal(err)
	}

}

func (xoc *CmdLine) Detach(rz *Realization) error {

	xoc.Config.Test.Logf("detaching %s", rz.Rzid())

	out, err := exec.Command("xdc", "detach").CombinedOutput()

	if err != nil {
		return fmt.Errorf("detach failed: %v: %s", err, string(out))
	}

	return nil

}

func (xoc *CmdLineFatal) Detach(rz *Realization) {

	err := xoc.cmd.Detach(rz)
	if err != nil {
		xoc.cmd.Config.Test.Fatal(err)
	}

}

func (xoc *CmdLine) Ping(node string, retries int) error {

	xoc.Config.Test.Logf("pinging %s", node)

	var err error
	var out []byte
	for i := 0; i < retries; i++ {
		out, err = exec.Command(
			"ping", "-c", "2", "-W", "2", node).CombinedOutput()
		if err == nil {
			break
		}
		time.Sleep(5 * time.Second)
	}

	if err != nil {
		return fmt.Errorf("ping failed: %v: %s", err, string(out))
	}

	return nil

}

func (xoc *CmdLineFatal) Ping(node string, retries int) {

	err := xoc.cmd.Ping(node, retries)
	if err != nil {
		xoc.cmd.Config.Test.Fatal(err)
	}

}

// CLI (mergetb) commands will exit with a non-zero code on "failure". This
// may not be an execution failure, but an API "failure" (like
// showing a non-existant experiment). If this is a MergeError
// then parse the output, regenerate the MergeError and return
// that.
func (xoc *CmdLine) handleCmdError(out []byte, err error) error {

	if len(out) == 0 { // pass through error
		return err
	}

	// If the output is a Model Error (aka an API error), pass that back
	me, err := merror.FromModelError(out)
	if err != nil {
		// :shrug: unable to parse the output into mergeerror form
		return err
	}

	return me
}

func (xoc *CmdLine) unwind(f func()) {

	xoc.unwindStack = append([]func(){f}, xoc.unwindStack...)

}

func (xoc *CmdLine) Unwind() {

	xoc.Config.Test.Logf("unwinding")

	for _, f := range xoc.unwindStack {
		f()
	}

}

func (xoc *CmdLineFatal) Unwind() {

	xoc.cmd.Unwind()

}
