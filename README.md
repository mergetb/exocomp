# The Exocomp Testing Platform

This is a testing platform for Merge. Tests are executed by elements called
Exocomps. It's assumed that

- Exocomps execute in a standard Go testing context
- Exocomps execute on an XDC
- Exocomps can access the Merge Portal Admin API (has API cert)
- Exocomps can access Merge Facility Admin APIs (has API cert)

An Exocomp test looks like this

```go
func TestTriforce(t *testing.T) {

	// Define the context the Exocomp will execute within
	cfg := exocomp.Config{
		Test:     t,
		User:     "reg",
		Password: "endicott^2",
		Project:  "reg",
	}

	// CmdLineFatal is an exocomp that stops immediately on the first error.
	xoc := exocomp.NewCmdLineFatal(cfg)

	// Initialization does basic setup like user login
	xoc.Init()
	defer xoc.Unwind() // Always unwind whatever this test has done

	// This test creates, realizes and materializes an experiment and pings each
	// node
	xp := xoc.NewExperiment("triforce")
	xoc.Push(xp, "triforce.py")
	rz := xoc.Realize(xp, "one")

	xoc.Materialize(rz)
	xoc.Wait(rz, exocomp.Materialize)

	xoc.Attach(rz)

	for _, node := range xp.Net.Nodes {
		xoc.Ping(node.Id)
	}

}
```

The expected deployment model for an exocomp set of tests is

```
go test -c <my_test.go>
scp my.test <user>@<xdc>:
ssh <user>@<xdc>
./my.test
```

In time we'll build a deployment harness that can manage automatic build/deploy/test workflows onto XDCs.

There are currently two Exocomps

- `CmdLineFatal`: Internally uses the `mergetb` command line tool. This Exocomp
  Is for tests that should always pass. Fatals on first error.  Allows for
  writing very concise tests as there is no test-level error handling.
- `CmdLine`: Is is also implemented in terms of the `mergetb` command line tool
  However, `CmdLine` will not die on errors and return errors to the caller for
  handling. This is useful for tests that are design to probe failure
  conditions.

## TODO

- `Api` and `Api` fatal variants that use the Merge API directly instead of the
  command line client.
- `Admin` Exocomps that use the `ma` command line client and the Merge Admin
  API.

