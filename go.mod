module gitlab.com/mergetb/exocomp

go 1.14

require (
	github.com/sethvargo/go-password v0.2.0 // indirect
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.6.1
	gitlab.com/mergetb/engine v0.5.18-0.20201016225148-fa2c364db5d2
	gitlab.com/mergetb/mergeapi v0.1.13-0.20200904031959-8e722e8ddcea
	gitlab.com/mergetb/tech/cogs v0.8.14-0.20200927024554-7d47659aef37
	gitlab.com/mergetb/xir v0.2.20-0.20201117002058-9c2452d1f85f
	google.golang.org/grpc v1.33.1
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)

//replace gitlab.com/mergetb/xir => /space/virt/xir

//replace gitlab.com/mergetb/mergeapi => /space/virt/mergeapi

//replace gitlab.com/mergetb/engine => /space/virt/portal/services

//replace gitlab.com/mergetb/tech/cogs => /space/virt/cogs

replace gitlab.com/mergetb/engine => ../portal/services
