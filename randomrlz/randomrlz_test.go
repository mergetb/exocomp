package main

import (
	"flag"
	"fmt"
	"testing"

	ec "gitlab.com/mergetb/exocomp/pkg"
)

var (
	uid        string
	iterations int
)

func init() {
	flag.StringVar(&uid, "runas", "", "Portal account to run test as")
	flag.IntVar(&iterations, "iterations", 16, "Number of realization to do")
}

func TestRandomRlzs(t *testing.T) {

	// Run this under a different account for each concurrent instance.
	if uid == "" {
		t.Fatal("no runas account specified")
	}

	a := []*ec.Account{
		{
			Username: uid,
			Email:    fmt.Sprintf("%s@ufp.gov", uid),
			Password: "endicott^2",
		},
	}

	// boilerplate
	c := &ec.Components{
		Accounts: a,
	}

	c.InitComponents(t)
	defer c.PA.Unwind()
	defer c.Cli.Unwind()

	xp := c.Cli.NewExperiment("randrlz")
	for i := 0; i < iterations; i++ {
		c.Cli.Push(xp, "random-topo.py")
		rz := c.Cli.Realize(xp, fmt.Sprintf("rlz%d", i))
		c.PA.CheckAllocationTable()
		c.Cli.Free(rz)
		c.Cli.Wait(rz, ec.Free)
	}
}
