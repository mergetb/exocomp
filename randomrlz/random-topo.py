#!/usr/bin/env python3

from mergexp import *
from itertools import combinations
from random import random, choice

net = Network('randrlz')

def px(num):
    return net.node(
        f'p{num}',
        memory.capacity >= gb(1),
        proc.cores >= 1,
        disk.capacity >= gb(100),
        metal == True
    )

def vx(num):
    return net.node(
        f'v{num}',
        memory.capacity >= gb(1),
        proc.cores >= 1,
        disk.capacity >= gb(100),
    )

n=4         # number of nodes
edgep=0.3   # chance of edge connection
virtp=0.5   # chance node is virt

vcnt=0
pcnt=0

nodes = []

for v in range(n):
    if random() > virtp:
        nodes.append(px(pcnt))
        pcnt += 1
    else:
        nodes.append(vx(vcnt))
        vcnt += 1

netcnt = 1
for c in combinations(nodes, 2):
    if random() < edgep:
        l = net.connect([c[0], c[1]])
        l[c[0]].socket.addrs.extend(ip4(f"10.0.{netcnt}.1/24"))
        l[c[1]].socket.addrs.extend(ip4(f"10.0.{netcnt}.2/24"))
        netcnt += 1

# attach disconnected nodes                                     
for node in nodes:
    if len(node.sockets) == 0:
        # print(f'attaching {node.spec.id}')
        nbr = choice(nodes)
        while nbr.spec.id != node.spec.id:
            nbr = choice(nodes)
        l = net.connect([node, nbr])
        l[node].socket.addrs.extend(ip4(f"10.0.{netcnt}.1/24"))
        l[nbr].socket.addrs.extend(ip4(f"10.0.{netcnt}.2/24"))
        netcnt += 1

# xx = net.xir()
# print(xx)

experiment(net)

