package main

import (
	"testing"
	//"time"

	"gitlab.com/mergetb/exocomp/pkg"
)

func TestTriforce(t *testing.T) {

	// boilerplate
	c := &exocomp.Components{}
	c.InitComponents(t)
	defer c.PA.Unwind()
	defer c.Cli.Unwind()

	// create experiment

	xp := c.Cli.NewExperiment("triforce")
	c.Cli.Push(xp, "triforce.py")

	// realize and check consistency

	rz := c.Cli.Realize(xp, "one")
	c.PA.CheckAllocationTable()

	// materialize attach and verify node access

	mz := c.Cli.Materialize(rz)
	c.Cli.Wait(rz, exocomp.Materialize)

	c.Cli.Attach(rz)

	for _, node := range xp.Net.Nodes {
		c.Cli.Ping(node.Id, 10)
	}

	c.Cli.Detach(rz)

	// demat and verify VMs are gone

	c.Cli.Dematerialize(mz)
	c.Cli.Wait(rz, exocomp.Dematerialize)
	c.FA.CheckNoVMs(rz.Rzid())

	t.Logf("freeing")
	c.Cli.Free(rz)
	c.PA.CheckNoAllocs(rz.Rzid())
}
