package main

import (
	"errors"
	"testing"

	"gitlab.com/mergetb/engine/pkg/merror"
	ec "gitlab.com/mergetb/exocomp/pkg"
)

func TestExperimentCrud(t *testing.T) {

	// boilerplate
	c := &ec.Components{}
	c.InitComponents(t)
	defer c.PA.Unwind()
	defer c.Cli.Unwind()

	names := []string{"blarfyexp", "blrogla", "hello"}

	for _, name := range names {
		c.Cli.NewExperiment(name)
		exp, err := c.Cli.Cmd().ShowExperiment(name)
		if err != nil {
			t.Fatal(err)
		}

		c.Assert.Equal(exp.Name, name)
		c.Assert.Equal(exp.Project, c.Cli.Cmd().Config.Project)
	}
}

// func TestProjectMembers(t *testing.T) {
//
// 	// boilerplate
// 	c := &ec.Components{}
// 	c.InitComponents(t)
// 	defer c.PA.Unwind()
// 	defer c.Cli.Unwind()
//
// 	pid := c.Cli.Cmd().Config.Project // ugh
//
// }

func TestNonExistentThings(t *testing.T) {

	// boilerplate
	c := &ec.Components{}
	c.InitComponents(t)
	defer c.PA.Unwind()
	defer c.Cli.Unwind()

	_, err := c.Cli.Cmd().ShowExperiment("nopenotanexperimentname")
	// This doesn't seem to exist for some reason
	// c.Assert.ErrorIs(err, merror.ErrNotFound)
	if !errors.Is(err, merror.ErrNotFound) {
		t.Fatal(err)
	}
}
