package main

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	"github.com/sethvargo/go-password/password"
	"gitlab.com/mergetb/exocomp/pkg"
)

type User struct {
	Name     string
	Password string
	Email    string
	Token    string
}

// Create a set of identities and user accounts based on those identities,
// concurrently with random interleaving.
func TestUserFury(t *testing.T) {

	//TODO N := 1000
	N := 1

	var users []*User
	for i := 0; i < N; i++ {
		users = append(users, defineUser(t, i))
	}

	regCh := make(chan error)
	createCh := make(chan error)

	for _, user := range users {

		go func(user *User) {
			xpu, err := registerId(t, user)
			if err != nil {
				defer xpu.Unwind()
			}
			regCh <- err
			createCh <- loginUser(t, user)
		}(user)

	}

	for _ = range users {

		err := <-regCh
		if err != nil {
			t.Error(err)
		}

		err = <-createCh
		if err != nil {
			t.Error(err)
		}

	}

	xpu := exocomp.NewXpUnit(
		exocomp.Config{
			Server: "api.mergetb.example.net",
			Port:   6000,
		},
	)
	err := xpu.Login("ops", "4c9k8TUv2fj01Rq6PH5sdY7CzVJQibN3")
	if err != nil {
		t.Fatal(err)
	}

	accounts, err := xpu.GetUsers()
	if err != nil {
		t.Fatal(err)
	}
	if len(accounts) != N {
		t.Errorf("Expected %d accounts, found %d", N, len(accounts))
	}

}

func defineUser(t *testing.T, i int) *User {

	passwd, err := password.Generate(16, 4, 0, false, false)
	if err != nil {
		t.Fatal(err)
	}

	username := fmt.Sprintf("user%d", i)
	email := fmt.Sprintf("%s@example.net", username)

	return &User{
		Name:     username,
		Password: passwd,
		Email:    email,
	}

}

func registerId(t *testing.T, user *User) (*exocomp.XpUnit, error) {

	time.Sleep(time.Duration(rand.Intn(5000)) * time.Millisecond)

	xpu := exocomp.NewXpUnit(
		exocomp.Config{
			Server: "api.mergetb.example.net",
			Port:   6000,
		},
	)

	return xpu, xpu.Register(user.Name, user.Email, user.Password)

}

func loginUser(t *testing.T, user *User) error {

	time.Sleep(time.Duration(rand.Intn(5000)) * time.Millisecond)

	xpu := exocomp.NewXpUnit(
		exocomp.Config{
			Server: "api.mergetb.example.net",
			Port:   6000,
		},
	)

	err := xpu.Login(user.Name, user.Password)
	if err != nil {
		return err
	}

	return nil

}
